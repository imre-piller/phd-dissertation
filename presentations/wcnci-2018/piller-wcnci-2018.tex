\documentclass[14pt]{beamer}

\usetheme{Luebeck}
\usecolortheme{whale}

% \setbeamertemplate{navigation symbols}{}

\frenchspacing

% Language packages
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
% \usepackage[magyar]{babel}

% AMS
\usepackage{amssymb,amsmath}

% Graphic packages
\usepackage{graphicx}

% Syntax highlighting
\usepackage{listings}
\usepackage{python}

\title{RDF Graph Representation of Logical Formulas}
\author[Imre Piller]{Imre Piller}
\institute{University of Miskolc}
\date{WCNCI, Kecskemét, \\ October 16, 2018}
%\date{\today}

\begin{document}

%------------
\begin{frame}
\titlepage
\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Overview}

\begin{itemize}
\item Knowledge representation models
\item FCA related representations
\item Predicate properties
\item Logical operators and rules
\item Structural optimization
\item Advantages, disadvantages
\end{itemize}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{RDF - Resource Description Framework}

\begin{itemize}
\item W3C standard for constantly changing data
\item Statements of resources as triplets
\item \emph{subject-predicate-object}
\item RDFS for schema definition
\end{itemize}

\medskip

\emph{Our main goal is to provide a bijection between formal contexts and RDF graphs.}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{OWL - Web Ontology Language}

\begin{itemize}
\item Extends the schemas with semantics
\item DL Axioms (\textit{Descriptive Logic})
\item It provides explicit information for reasoning
\end{itemize}

\medskip

\emph{Instead of new definitions we try to represent all concepts and rules in the semantic graph itself.}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Multicontext Representation}

Let $R$ the set of resources and $P$ the set of predicates. We can define the semantic graph as
\[
\mathbb{G} = \left\{
(s, p, o) | s, o \in R, p \in P
\right\}
\Leftrightarrow
\mathbb{G} \subseteq R \times P \times R.
\]
Let $P = \{p_1, p_2, \ldots, p_n\}$. We can consider the graph as the set of formal contexts \(\mathbb{K}_1, \ldots, \mathbb{K}_n\), where
\[
\mathbb{K}_i = \left\{
(s, o) | s, o \in R, (s, p_i, o) \in \mathbb{G}
\right\}.
\]

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Triadic Concept Representation}

Let define a triadic formal concept as \(\mathbb{K} = (G, M, B, I)\), where the \(G\) is the set of objects, \(M\) is the set of attributes, and \(B\) is the set of conditions. \(I\) is a ternary relation \(I \subseteq G \times M \times B\).

\bigskip

Formally, the \(\mathbb{G}\) semantic graph is similar to a triconcept \(\mathbb{K} = (R, P, R, I)\), where
\[
(s, p, o) \in I
\quad \Leftrightarrow \quad
(s, p, o) \in \mathbb{G}.
\]

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Predicate properties}

\begin{itemize}
\item The predicates are relations.
\item We analyze the properties of predicates.
\item In fact, we consider the relations of relations.
\end{itemize}

\bigskip

Frequently used properties
\begin{itemize}
\item reflexivity
\item symmetricity/anti-symmetricity
\item transitivity
\end{itemize}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Symmetricity}

Let assume that $p \in P$ is a symmetric predicate. Then
\(
(s, p, o) \in \mathbb{G}
\Leftrightarrow
(o, p, s) \in \mathbb{G}
\).

\medskip

Let define an indicator function for predicate $p$:
\[
\chi_p(s, o) = \begin{cases}
1, & \text{if } (s, p, o) \in \mathbb{G}, \\
0, & \text{if } (s, p, o) \notin \mathbb{G}.
\end{cases}
\]
We can represent symmetricity as
\[
(\chi_p(s, o) \wedge \chi_p(o, s)) \vee
(\overline{\chi_p(s, o)} \wedge \overline{\chi_p(o, s)}).
\]

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Anti-symmetricity}

Let consider an anti-symmetric predicate $p \in P$.
Then \((s, p, o) \in \mathbb{G} \Rightarrow (o, p, s) \notin \mathbb{G}\).

\bigskip

The $p$ is anti-symmetric when the logical formula
\[
\overline{\chi_p(s, o) \wedge \chi_p(o, s)}
\]
is true for all possible $s, o \in R$.

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Transitivity}

Let $a, b, c \in R$, and $p \in P$. The $p$ is transitive, when
\[
(a, p, b), (b, p, c) \in \mathbb{G}
\Rightarrow
(a, p, c) \in \mathbb{G}.
\]

\medskip

It also can be formulated as
\[
\overline{\chi_p(a, b) \wedge \chi_p(b, c)} \vee
\chi_p(a, c).
\]

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Property representation}

It is easy to see the followings.
\begin{itemize}
\item We can represent all possible property as logical formula.
\item The number of possible predicate properties is infinite.
\item The semantic graph is able to represent these properties.
\end{itemize}

\bigskip

\emph{Our goal is to represent these logical formulas as the set of statements.}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Logical operators and predicates}

We have to define some logical operators.
\begin{itemize}
\item We can use $\wedge, \vee$ and negation operators. \\
(There are further possibilities.)
\item Unary operator: \texttt{op}, \texttt{arg}
\item Binary operator: \texttt{op}, \texttt{arg1}, \texttt{arg2}
\end{itemize}

\bigskip

Predicates
\begin{itemize}
\item In RDF the predicate is a name.
\item We can use these names as arguments.
\item We can define a predicate as a resource.
\end{itemize}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Example: $\overline{x \wedge \overline{y}} \vee z$}

\begin{figure}
\centering
\includegraphics[scale=0.6]{images/example.png}
\end{figure}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Statements of the Example}

\begin{verbatim}
_1 arg1 _2.    _3 arg1 x.
_1 op OR.      _3 op AND.
_1 arg2 z.     _3 arg2 _4.
_2 op NOT.     _4 op NOT.
_2 arg _3.     _4 arg y.
\end{verbatim}

\emph{We have used \texttt{\_1}, \texttt{\_2}, \texttt{\_3} and \texttt{\_4} instead of blank nodes.}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{List of the Rules}

We would like to consider multiple rules at the same time.
\begin{itemize}
\item We can use a linked-list pattern (\texttt{next}, \texttt{value}).
\item It is enough to join the rules by conjunction.
\end{itemize}

\begin{figure}
\centering
\includegraphics[scale=0.7]{images/linked.png}
\end{figure}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Implicit and Explicit Information}

\begin{itemize}
\item The RDF graph contains only explicit information.
\item We supplement the described knowledge with implicit information.
\end{itemize}

\emph{We try to minimize the amount of implicit information, without making the knowledge representation unnecessary complex.}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Structural Optimization}

Let consider a subgraph with $n$ resources. Let assume that they are in an equivalence class.
\begin{itemize}
\item It requires at least $n - 1$ statements.
\item For explicit representation we need $\dfrac{n \cdot (n - 1)}{2}$ statements.
\item It requires approximately $n$ statement operations for any insertion/deletion.
\end{itemize}

\emph{We can describe this higher level concept in the RDF graph explicitly, without large number of statements and operations.}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Transparent Simplification}

\begin{itemize}
\item We would like to work directly with the statements.
\item We prefer to store the simplified model.
\item It is beneficial in the sense of calculation and storage cost.
\end{itemize}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{Conclusions and Future Works}

\textbf{Conclusions}
\begin{itemize}
\item The proposed method is able to represent arbitrary predicate properties.
\item The knowledge representation format remains the same.
\item The complexity of the semantic graph is increasing.
\end{itemize}

\bigskip

\textbf{Further works}
\begin{itemize}
\item Check the relations of the predicates in the available ontologies.
\item Show detailed equivalences with the RDFs and OWL standards.
\end{itemize}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{References}

\begin{itemize}
\item[1] RDF, Semantic Web Standards, \texttt{https://www.w3.org/RDF}, W3C
\item[2] OWL, Semantic Web Standards, \texttt{https://www.w3.org/OWL}, W3C
\item[3] Lehmann, Fritz, and Rudolf Wille. \emph{A triadic approach to formal concept analysis}. International Conference on Conceptual Structures. Springer, Berlin, Heidelberg, 1995.
\end{itemize}

\end{frame}

%------------
\begin{frame}[fragile]
\frametitle{}

\begin{center}
\Large \textbf{Thank you for your kind attention!}
\end{center}

\end{frame}

\end{document}
