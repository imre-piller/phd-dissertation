\chapter{Bijection of RDF and FCA models}

% TODO: Check the possible bijections between the two models!

\section{Terminology and Notations}

We can consider the formal context as a simple ontology, where we have only one predicate. The FCA does not distinguish (in its original form) the types of relations between the objects and attributes. The ontologies provide types explicitly by predicates.

For further considerations we have to reformulate our notations. Unfortunately, the \textit{object} is a common term in the case of formal concepts and ontologies also, but its meaning is different. We would like to find analogies among the FCA and RDF concepts. We can see the related terms in Table \ref{tab:conversion}.

\begin{table}[h!]
\centering
\begin{tabular}{|l|l|}
\hline
FCA & RDF \\
\hline
object & subject \\
\hline
attribute & predicate \\
& object \\
\hline
\end{tabular}
\caption{Related terms in FCA and RDF}
\label{tab:conversion}
\end{table}

The \textit{concept} is a well defined term in formal concept analysis. We try to avoid ambiguity by introducing some simple notation. Instead of objects let call $r \in R$ a resource. Let $P$ the set of predicates. Therefore, we can define the formal context as $\mathbb{K} = (R, R, I)$, where $I \subseteq R \times R$. The triplets of the RDF graph can be represented as $\mathbb{G} = \{(x, p, y) \in R \times P \times R\}$.

\section{Transitive Association}

Let $\mathbb{G} = \{(x_1, p_1, x_2), (x_2, p_2, x_3)\}$ a graph, where $x_1, x_2, x_3 \in R, p_1, p_2 \in P$ (Figure \ref{fig:trans_assoc}). We consider how the $x_1$ and $x_3$ can be related.

\begin{figure}[h!]
\centering
\begin{tikzpicture}
\node[draw, circle] (x1) at (-2, 0) {$x_1$};
\node[draw, circle] (x2) at (0, 0) {$x_2$};
\node[draw, circle] (x3) at (2, 0) {$x_3$};
\draw[->] (x1) edge node [above] {$p_1$} (x2);
\draw[->] (x2) edge node [above] {$p_2$} (x3);
\end{tikzpicture}
\caption{Transitive association}
\label{fig:trans_assoc}
\end{figure}

When there is no additional information about the transitivity of $p_1$ and $p_2$ predicates we have the following options.
\begin{itemize}
\item We ignore all of these types of association.
\item We consider all transitions which means that we calculate the transitive clojure of the incidence relation.
\end{itemize}
We concentrate only to the exact approaches. Some researches estimate the strength of the association by statistical methods based on frequencies. It is out of the topic of this work.

\section{Bijection between the Representations}

We assume that the underlying model of the RDF and FCA representations is the same. For instance, we would like to represent the same knowledge in two ways.

Let $\mathbb{K}_1 = (G, M, I_1), \ldots, \mathbb{K}_n = (G, M, I_n)$ formal contexts. Let assume that $G \cap M = \emptyset$. Let define the set of predicates as $P = \{p_1, \ldots, p_n\}$. All $p_i$ predicates have the corresponding $I_i$ incidence matrix. The resulted graph is
$$
\mathbb{G} = \{(x, p_i, y) | x, y \in R, p_i \in P,(x, y) \in I_i\}.
$$
It is easy to see that this mapping between the $\mathbb{K}_1, \ldots, \mathbb{K}_n$ and the $\mathbb{G}$ graph is a bijection, because 
$$
(x, p_i, y) \in \mathbb{G} \quad \Leftrightarrow \quad x \in G, y \in M.
$$
It means that we can construct $n$ concept lattices for a graph, where $n = |P|$.

The $G \cap M = \emptyset$ guarantees that there is no possible transitivity in the graph. It means also that the RDF graph is a bipartite graph.

Let check the case when $G \cap M \neq \emptyset$. We consider the following graphs:
\begin{align*}
\mathbb{G}_1 &= \{(x_1, p, x_2), (x_2, p, x_3)\}, \\
\mathbb{G}_2 &= \{(x_1, p, x_2), (x_2, p, x_3), (x_1, p, x_3)\}.
\end{align*}
In the case of $\mathbb{G}_1$ we have to decide about the transitivity of $p$. When we assume that $p$ is not a transitive property we get
$$
\mathbb{K}_1 = (\{x_1, x_2\}, \{x_2, x_3\}, \{(x_1, x_2), (x_2, x_3)\}).
$$
When $p$ is considered as a transitive predicate
$$
\mathbb{K}'_1 = (\{x_1, x_2\}, \{x_2, x_3\}, \{(x_1, x_2), (x_2, x_3), (x_1, x_3)\}).
$$
In the last case $\mathbb{K}'_1$ equals to the corresponding formal context of $\mathbb{G}_2$, because that graph explicitly contains the transitions.

\section{Representation of Homonyms}

Let consider $\mathbb{K}_1 = (G, M_1, I_1), \ldots, \mathbb{K}_n = (G, M_n, I_n)$ formal contexts. Let assume that $\exists i, j, (i \neq j), M_i \cap M_j \neq \emptyset$. Moreover, $y \in M_i, y \in M_j$ has different meaning in $\mathbb{K}_i$ and $\mathbb{K}_j$ contexts. We called $y$ as a \textit{homonym}.

When we know that the $y$ is a homonym, the representation in the RDF graph must be different.

Let introduce a $q \in P$ predicate. It necessary for decouple the name from the meaning in the graph. (For example, when the $(a, q, b)$ triplet is in the graph, we can say that \textit{the resource a is called b}.)

For example, let consider the following formal contexts:
\begin{align*}
\mathbb{K}_1 &= (\{x_1\}, \{y\}, \{(x_1, y)\}) \\
\mathbb{K}_2 &= (\{x_2\}, \{y\}, \{(x_2, y)\}).
\end{align*}

When we do not distinguish the meaning of $y$ in the different contexts, the graph is
$$
\mathbb{G} = \{(x_1, p, y), (x_2, p, y)\}.
$$

In other case, we must introduce new resources for different meanings as $y_1, y_2 \in R$. The graph
$$
\mathbb{G}' = \{
(x_1, p, y_1), (x_2, p, y_2), (y_1, q, y), (y_2, q, y)
\}
$$
contains this information while keeps the original names (Figure \ref{fig:homonyme_graph}).
\begin{figure}[h!]
\centering
\begin{tikzpicture}
\node[draw, circle] (x1) at (-2, 1) {$x_1$};
\node[draw, circle] (x2) at (-2, -1) {$x_2$};
\node[draw, circle] (y1) at (0, 1) {$y_1$};
\node[draw, circle] (y2) at (0, -1) {$y_2$};
\node[draw, circle] (y) at (2, 0) {$y$};
\draw[->] (x1) edge node [above] {$p$} (y1);
\draw[->] (x2) edge node [above] {$p$} (y2);
\draw[->] (y1) edge node [above] {$q$} (y);
\draw[->] (y2) edge node [above] {$q$} (y);
\end{tikzpicture}
\caption{Homonyme graph}
\label{fig:homonyme_graph}
\end{figure}
Let try to determine the $\mathbb{K}'_p$ and $\mathbb{K}'_q$ formal contexts from $\mathbb{G}'$:
\begin{align*}
\mathbb{K}'_p &= (\{x_1, x_2\}, \{y_1, y_2\}, \{(x_1, y_1), (x_2, y_2)\}), \\
\mathbb{K}'_q &= (\{y_1, y_2\}, \{y\}, \{(y_1, y), (y_2, y)\}).
\end{align*}
The bijection also holds without and with the consideration of homonyms:
\begin{align*}
\{\mathbb{K}_1, \mathbb{K}_2\} &\leftrightarrow \mathbb{G} \\
\{\mathbb{K}_1, \mathbb{K}_2\} &\rightarrow \mathbb{G}' \leftrightarrow \{\mathbb{K}'_p, \mathbb{K}'_q\}.
\end{align*}
We have to emphasize that in the second case we have used additional information, which means that the underlying models are different.

\section{Representation of Synonyms}

Let assume that we have $y_1, \ldots, y_k \in R$ different names with exactly the same meaning. We can decouple the names from the meaning by the following way. Let the original formal context is
$$
\mathbb{K} = \left(\{x\}, \{y_1, \ldots, y_k\}, \{(x, y_1), \ldots, (x, y_k)\}\right).
$$
Let introduce $x_m \in R$ as the meaning of $x \in R$. By using the $q \in P$ predicate for name association we get the
$$
\mathbb{G} = \{(x, p, x_m), (x_m, q, y_1), \ldots, (x_m, q, y_k)\}
$$
RDF graph, where $p \in P$ is an arbitrary predicate (Figure \ref{fig:synonyme_graph}).
\begin{figure}[h!]
\centering
\begin{tikzpicture}
\node[draw, circle] (x) at (-2, 0) {$x$};
\node[draw, circle] (xm) at (0, 0) {$x_m$};
\node[draw, circle] (y1) at (2, 1) {$y_1$};
\node[draw, circle] (yk) at (2, -1) {$y_k$};
\draw[->] (x) edge node [above] {$p$} (xm);
\draw[->] (xm) edge node [above] {$q$} (y1);
\draw[->] (xm) edge node [above] {$q$} (yk);
\path (y1) -- (yk) node [midway, sloped] {$\ldots$};
\end{tikzpicture}
\caption{Synonyme graph}
\label{fig:synonyme_graph}
\end{figure}
For the proper formal context representation we have to introduce the $\mathbb{K}_p$ and the $\mathbb{K}_q$ formal contexts for $p, q \in P$ predicates:
\begin{align*}
\mathbb{K}_p &= (\{x\}, \{x_m\}, \{(x, x_m)\}), \\
\mathbb{K}_q &= (\{x_m\}, \{y_1, \ldots, y_k\}, \{(x_m, y_1), \ldots, (x_m, y_k)\}).
\end{align*}
The bijection only holds for the altered contexts.
\begin{align*}
\mathbb{K} \rightarrow &\{\mathbb{K}_p, \mathbb{K}_q\} \\
&\{\mathbb{K}_p, \mathbb{K}_q\} \leftrightarrow \mathbb{G}
\end{align*}

In general, the synonyms does not work in this strict sense. For instance, let consider the case of words \textit{program}, \textit{application}, \textit{app}, \textit{software} and \textit{SW}. These words do not mean exactly the same thing. We can consider some types of inclusion between them by the following statements.
\begin{itemize}
\item \textit{An application is a program.}
\item \textit{A program is a software.}
\item \textit{The word app means application.}
\item \textit{The SW abbreviates the word software.}
\end{itemize}
We have to note that these statements have chosen intuitively, because the words \textit{application} and \textit{program} are not exclusively terms of the information science.

In this example we have three different higher level terms, as $x_1, x_2, x_3 \in R$. The resulted RDF graph is
\begin{gather*}
\mathbb{G} = \{
(x_1, \textit{called}, \text{application}),
(x_1, \textit{called}, \text{app}),
(\text{app}, \textit{means}, \text{application}),
(x_1, \textit{is\_a}, x_2), \\
(x_2, \textit{called}, \text{program}),
(x_2, \textit{is\_a}, x_3),
(x_2, \textit{called}, \text{software}),
(x_2, \textit{called}, \text{SW}),
(\text{SW}, \textit{abbreviates}, \text{software})
\},
\end{gather*}
where
\begin{align*}
R &= \{
x_1, x_2, x_3,
\text{program}, \text{application}, \text{app}, \text{software}, \text{SW}
\}, \\
P &= \{
\textit{called}, \textit{means}, \textit{is\_a}, \textit{abbreviates}
\}.
\end{align*}
We can see the graph on Figure \ref{fig:software_graph}.
\begin{figure}[h!]
\centering
\begin{tikzpicture}
\node[draw, circle] (x1) at (-3, 0) {$x_1$};
\node[draw, circle] (x2) at (3, 0) {$x_2$};
\node[draw] (application) at (-4, -2) {application};
\node[draw] (app) at (-2, -2) {app};
\node[draw] (program) at (1, -2) {program};
\node[draw] (sw) at (3, -2) {sw};
\node[draw] (software) at (6, -2) {software};
\draw[->] (x1) edge node [midway, sloped, above] {is\_a} (x2);
\draw[->] (x1) edge node [midway, sloped, above] {called} (application);
\draw[->] (x1) edge node [midway, sloped, above] {called} (app);
\draw[->] (x2) edge node [midway, sloped, above] {called} (program);
\draw[->] (x2) edge node [midway, sloped, above] {called} (sw);
\draw[->] (x2) edge node [midway, sloped, above] {called} (software);
\draw[->] (sw) edge node [midway, sloped, above] {abbreviates} (software);
\end{tikzpicture}
\caption{Software graph}
\label{fig:software_graph}
\end{figure}
It is important to emphasize that, these type of mathematical models are flexible enough for approximating the underlying real world concepts, but we have to define their main building blocks. As in the previous example, we must know the actual meaning of the resources and predicates. The model is unable to associate any further information to the words by itself.

\section{Concepts and Queries}

The RDF graphs in this consideration describes the relations of subjects, predicates and objects. Our previously presented approaches tried to match the RDF model with the classical FCA model. It is easy to see that these relations can be represented by triadic concepts instead of the dyadic ones.

The triadic concept is a $(G, M, B, Y)$ quadruple, where the $B$ is the set of \textit{conditions} and $Y \in G \times M \times B$. We would like to work with predicates instead of the conditions. It results the $(R, P, R, \mathbb{G})$ triconcept, where $R$ is the set of resources, $P$ is the set of predicates and $\mathbb{G} \subseteq R \times P \times R$ is the RDF graph.

